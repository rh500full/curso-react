import React from 'react'

const Todoitem = ({todo,toggleTodo}) => {
   const {id, task, completed} = todo;
   
   const todoClick = ()=>{
      toggleTodo(id);
   }
   return (
      <li>
         <input type="checkbox" checked={completed} onChange={todoClick} />
         {task}
      </li>
   )
}

export default Todoitem
