import React,{useState, useRef, useEffect} from 'react';
import {v4 as uuid} from 'uuid';
import Todolist from './components/Todolist'

const KEY = 'todoApp.todos'
export function App(){
   const [todos,setTodos] = useState([
      {
         id:1,
         task:'tarea 1',
         completed: false
      }
   ]);

   const TaskRef = useRef();

   useEffect(()=>{
      const storedTodos = JSON.parse(localStorage.getItem(KEY))
      if(storedTodos){
         setTodos(storedTodos)
      }
   },[])

   useEffect(()=>{
      localStorage.setItem(KEY,JSON.stringify(todos))
   },[todos])

   const addTodo = ()=>{
      const task = TaskRef.current.value;
      if(task === '') return;

      setTodos((prev)=>{
         return [...prev,{id:uuid(),task,completed:false}]
      });

      TaskRef.current.value = null;
   }

   const removeTodos = ()=>{
      const newTodos = todos.filter((todo)=>!todo.completed);
      setTodos(newTodos);
   }

   const toggleTodo = (id)=>{
      const newTodos = [...todos];
      const todo = newTodos.find(todo => todo.id === id);
      todo.completed = !todo.completed;
      setTodos(newTodos);
   }
   return (
      <>
         <Todolist todos={todos} toggleTodo={toggleTodo}/>
         <input ref={TaskRef} type="text" className="" placeholder="Agregar Tarea" />
         <button onClick={addTodo}>+</button>
         <button onClick={removeTodos}>-</button>
         <div>Te quedan {todos.filter((todo)=> !todo.completed).length} tareas por completar</div>
      </>
   );
}
