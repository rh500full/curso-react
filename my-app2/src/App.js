import Parrafo from './components/Parrafo'
import Variables from './components/Variables';
import Eventos from './components/Eventos';
import Contador from './components/Contador';
import Lista from './components/Lista';
import Formulario from './components/Formulario';

function App() {
  return (
    <div className="container mt-5">
      {/* <Parrafo/>
      <Variables/>
      <Eventos/>
      <Contador/> */}
      <Formulario/>
      <Lista/>
    </div>
  );
}

export default App;
