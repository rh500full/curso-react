import React from 'react'

const Lista = () => {
   const [lista, setLista] = React.useState([]);
   
   const agregar = () => {
      setLista([
         ...lista,
      ])
   }
   return (
      <div>
         <h2>Lista</h2>
         {
            lista.map((item,index)=>(
               <h3 key={index}>{item.fruta} {item.descripcion}</h3>
            ))
         }
      </div>
   )
}

export default Lista
