import {React,useState} from 'react'

const Formulario = () => {
   const [fruta,setFruta] = useState();
   const [descripcion,setDescripcion] = useState();
   const [lista, setLista] = useState([]);

   const guardarDatos = (e) => {
      e.preventDefault();
      if(!fruta.trim()){
         console.log("falta fruta");
      }
      if(!descripcion.trim()){
         console.log("falta descripcion");
      }

      setLista([
         ...lista,
         {
            fruta,
            descripcion,
         }
      ])
   }
   return (
      <div>
         <h3>Formulario</h3>
         <form onSubmit={guardarDatos}>
            <input type="text" className="form-control mb-2" placeholder="Ingrese fruta" onChange={(e) => setFruta(e.target.value)}/>
            <input type="text" className="form-control mb-2" placeholder="Descripcion" onChange={(e) => setDescripcion(e.target.value)}/>
            <button className="btn btn-primary btn-block">Agregar</button>
         </form>
      </div>
   )
}

export default Formulario
