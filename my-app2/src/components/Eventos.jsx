import React, { Fragment, useState } from 'react'

const Eventos = () => {
   const [text,setText] = useState('Texto por defecto')
   const eventoClick = ()=>{
      console.log("click en el boton");
      setText('cambiando')
   }
   return (
      <Fragment>
         <hr></hr>
         <h2>{text}</h2>
         <button onClick={eventoClick}>Click</button>
      </Fragment>
   )
}

export default Eventos
