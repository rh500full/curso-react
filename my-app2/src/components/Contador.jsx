import React from 'react'

const Contador = () => {
   const [contador, setContador] = React.useState(0);

   const aumentar = ()=>{
      setContador(contador+1)
   };
   return (
      <div>
         <h2>Contador: {contador}</h2>
         <h3>
            {
               contador > 3 ? 'Es mayor' : 'Es menor'
            }
         </h3>
         <button onClick={()=> aumentar()}>Aumentar</button>
      </div>
   )
}

export default Contador
